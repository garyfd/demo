import React from 'react'

const Templates_Loading = () => (
    <div className="loading">
        <img src="/assets/img/loading.gif" alt="Loading..." />
    </div>
)

export default Templates_Loading