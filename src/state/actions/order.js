import axios from 'axios'

export const loadOrder = (id) => {
    return (dispatch) => {
        axios.get(`https://us-central1-fddemo-5c0cd.cloudfunctions.net/load?id=${id}`)
            .then(response => {
                let data = response.data.order
                dispatch(setOrder(data))
                dispatch(orderLoaded())
            })
            .catch(err => {
                console.log('Error loading order: ', err)
            })
    }
}

export const createOrder = () => {
    return (dispatch) => {
        axios.get('https://us-central1-fddemo-5c0cd.cloudfunctions.net/create')
            .then(response => {
                let id = response.data.id
                dispatch(setOrder({id, total: 0}))
                dispatch(orderLoaded())
            })
            .catch(err => console.log('Error creating order: ', err))
    }
}

export const saveOrder = () => {
    return (dispatch, getState) => {
        let order = getState().order
        let { loaded, ...postData } = order
        
        axios.post(`https://us-central1-fddemo-5c0cd.cloudfunctions.net/save?id=${postData.id}`, postData)
            .catch(err => console.log('Error saving order: ', err))
    }
}

export const setOrder = data => {
    return {
        type: 'SET_ORDER',
        data
    }
}

export const orderLoaded = () => {
    return {
        type: 'ORDER_LOADED'
    }
}

export const selectBar = id => {
    return (dispatch) => {
        dispatch({ type: 'SET_BAR', id })
        dispatch(saveOrder())
    }
}

export const addDrink = drink => {
    return (dispatch) => {
        dispatch({ type: 'ADD_DRINK', drink })
        dispatch(saveOrder())
    }
}

export const removeDrink = drink => {
    return (dispatch) => {
        dispatch({ type: 'REMOVE_DRINK', drink })
        dispatch(saveOrder())
    }
}

