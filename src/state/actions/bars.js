import axios from 'axios'

export const loadBars = () => {
    return (dispatch) => {
        axios.get('https://us-central1-fddemo-5c0cd.cloudfunctions.net/bars')
            .then(response => {
                let bars = response.data.bars
                dispatch({ type: 'LOAD_BARS', bars })
            })
            .catch(err => {
                console.log('Error loading bars: ', err)
            })
    }
}