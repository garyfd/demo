import axios from 'axios'

export const loadDrinks = (bar) => {
    return (dispatch) => {
        axios.get(`https://us-central1-fddemo-5c0cd.cloudfunctions.net/drinks?bar=${bar}`)
            .then(response => {
                let drinks = response.data.drinks
                dispatch({ type: 'LOAD_DRINKS', drinks })
            })
            .catch(err => {
                console.log('Error loading drinks: ', err)
            })
    }
}