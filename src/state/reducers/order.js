let initialState = {
    id: null,
    bar: null,
    total: 0,
    drinks: [],
    loaded: false,
}

function reducer(state = initialState, action) {
    switch (action.type) {

        case 'SET_ORDER':
            return {
                ...state,
                ...action.data
            }
        
        case 'ORDER_LOADED':
            return {
                ...state,
                loaded: true
            }
            
        case 'SET_BAR':
            return {
                ...state,
                bar: action.id,
                total: 0,
                drinks: []
            }
        
        case 'ADD_DRINK':
            return {
                ...state,
                total: state.total + action.drink.current_price,
                drinks: [
                    ...state.drinks,
                    {
                        ...action.drink,
                        id: Date.now()
                    }
                ]
            }
        
        case 'REMOVE_DRINK':
            return {
                ...state,
                total: state.total - action.drink.current_price,
                drinks: state.drinks.filter(d => d.id !== action.drink.id)
            }

        default:
            return state
    }
}

export default reducer