let initialState = {
    drinks: {},
    loaded: false
}

function reducer(state = initialState, action) {
    switch (action.type) {

        case 'LOAD_DRINKS':
            return {
                drinks: action.drinks,
                loaded: true
            }
            
        default:
            return state
    }
}

export default reducer