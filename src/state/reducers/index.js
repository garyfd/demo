import { combineReducers } from 'redux'

import order from './order'
import bars from './bars'
import drinks from './drinks'

const reducers = combineReducers({
    order,
    bars,
    drinks
})

export default reducers