let initialState = {
    bars: {},
    loaded: false
}

function reducer(state = initialState, action) {
    switch (action.type) {

        case 'LOAD_BARS':
            return {
                bars: action.bars,
                loaded: true
            }
            
        default:
            return state
    }
}

export default reducer