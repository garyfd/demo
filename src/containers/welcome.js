import { connect } from 'react-redux'
import Components_Welcome from '../components/welcome'

import {
    createOrder
} from '../state/actions/order'

const mapStateToProps = ({ order }) => {
    return {
        order, 
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createOrder: () => dispatch(createOrder())
    }
}

const Containers_Welcome = connect(
    mapStateToProps,
    mapDispatchToProps
)(Components_Welcome)

export default Containers_Welcome