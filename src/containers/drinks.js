import { connect } from 'react-redux'
import Components_Drinks from '../components/drinks'

import {
    addDrink,
    removeDrink,
    loadOrder
} from '../state/actions/order'

import {
    loadDrinks
} from '../state/actions/drinks'

const mapStateToProps = ({ order, drinks }, { match }) => {
    return {
        order: {
            ...order,
            id: match.params.orderId
        },
        drinks,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadOrder:   (id)    => dispatch(loadOrder(id)),
        loadDrinks:  (bar)   => dispatch(loadDrinks(bar)),
        addDrink:    (drink) => dispatch(addDrink(drink)),
        removeDrink: (drink) => dispatch(removeDrink(drink))
    }
}

const Containers_Drinks = connect(
    mapStateToProps,
    mapDispatchToProps
)(Components_Drinks)

export default Containers_Drinks