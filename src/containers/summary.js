import { connect } from 'react-redux'
import Components_Summary from '../components/summary'

import {
    loadOrder
} from '../state/actions/order'

const mapStateToProps = ({ order }, { match }) => {
    return {
        order: {
            ...order,
            id: match.params.orderId
        }
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadOrder: (id) => dispatch(loadOrder(id)),
    }
}

const Containers_Summary = connect(
    mapStateToProps,
    mapDispatchToProps
)(Components_Summary)

export default Containers_Summary