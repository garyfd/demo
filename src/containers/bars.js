import { connect } from 'react-redux'
import Components_Bars from '../components/bars'

import {
    loadOrder,
    selectBar
} from '../state/actions/order'

import {
    loadBars
} from '../state/actions/bars'

const mapStateToProps = ({ order, bars }, { match }) => {
    return {
        bars,
        order: {
            ...order,
            id: match.params.orderId
        }
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadOrder: (id) => dispatch(loadOrder(id)),
        loadBars:  ()   => dispatch(loadBars()),
        selectBar: (id) => dispatch(selectBar(id))
    }
}

const Containers_Bars = connect(
    mapStateToProps,
    mapDispatchToProps
)(Components_Bars)

export default Containers_Bars