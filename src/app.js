import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import Routes from './routes'
import Store from './state/store'

const App = () => (
    <Provider store={Store}>
        <Routes />
    </Provider>
)

ReactDOM.render(
    <App />,
    document.getElementById('ReactApp')
)