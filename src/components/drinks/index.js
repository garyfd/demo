import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import Templates_Loading from '../../templates/loading'
import Components_Drinks_List from './list'

class Components_Bars extends Component {

    constructor(props) {
        super(props)

        this.state = {
            loaded: false,
        }
    }

    componentWillMount() {
        if(!this.props.order.loaded) {
            this.loadOrder()
        } else {
            this.loadDrinks()
        }
    }

    loadOrder() {
        this.props.loadOrder(this.props.order.id)
    }

    loadDrinks() {
        this.props.loadDrinks(this.props.order.bar)
    }

    componentWillReceiveProps(newProps) {
        if(!newProps.drinks.loaded) return this.loadDrinks()
        this.setState({ loaded: true })
    }

    render() {

        if(!this.state.loaded) return <Templates_Loading />

        return (
            <div>
                <h1>Select drinks</h1>

                <Components_Drinks_List
                    data={this.props.drinks.drinks}
                    onClick={this.props.addDrink}
                    action="Add"
                />

                {this.props.order.drinks.length > 0 && <div>
                    <hr />
                    <h4>Your Order</h4>
                    <Components_Drinks_List
                        data={this.props.order.drinks}
                        onClick={this.props.removeDrink}
                        action="Remove"
                    />
                    
                    <Link
                        to={`/summary/${this.props.order.id}`}
                        className="btn btn-primary"
                    >View Order Summary - £{this.props.order.total.toFixed(2)}</Link>
                </div>}
            </div>
        )
    }
}

export default Components_Bars