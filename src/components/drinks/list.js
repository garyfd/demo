import React from 'react'

const Components_Drinks_List = ({ data, onClick, action }) => (
    <ul className="list-group">
        {data.map((drink, i) => (
            <li key={i} className="list-group-item">
                £{drink.current_price.toFixed(2)} - {drink.name}
                <a
                    onClick={onClick.bind(null, drink)}
                    className="drinks__list__action"
                >{action}</a>
            </li>
        ))}
    </ul>
)

export default Components_Drinks_List