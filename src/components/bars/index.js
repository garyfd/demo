import React, { Component } from 'react'

import Components_Bars_List from './list'
import Components_Bars_Search from './search'
import Components_Bars_Next_Button from './nextButton'
import Templates_Loading from '../../templates/loading'

class Components_Bars extends Component {

    constructor(props) {
        super(props)

        this.state = {
            loaded: false,
            filter: ""
        }

        this.filterBars = this.filterBars.bind(this)
    }

    componentWillMount() {
        if(!this.props.order.loaded) this.props.loadOrder(this.props.order.id)
        this.props.loadBars()
    }

    componentWillReceiveProps(newProps) {
        if(newProps.bars.loaded && newProps.order.loaded) {
            this.setState({ loaded: true })
        }
    }

    filterBars(e) {
        this.setState({ filter: e.target.value })
    }

    render() {

        if(!this.state.loaded) return <Templates_Loading />

        let filteredBars = this.props.bars.bars.filter((bar) => {
            let name = bar.name.toLowerCase()
            let filter = this.state.filter.toLowerCase()
            return name.indexOf(filter) > -1
        })

        return (
            <div>

                <h1>Select Bar</h1>
                
                <Components_Bars_Search
                    filterBars={this.filterBars}
                />

                <Components_Bars_List
                    data={filteredBars}
                    selected={this.props.order.bar}
                    selectBar={this.props.selectBar}
                />

                <Components_Bars_Next_Button
                    data={this.props.order}
                />
            </div>
        )
    }
}

export default Components_Bars