import React from 'react'
import { Link } from 'react-router-dom'

const Components_Bars_Next_Button = ({ data }) => (
    <div>
        {data.bar && 
            <Link
                to={`/drinks/${data.id}`}
                className="btn btn-primary"
            >Select Drinks</Link>
        }
    </div>
)

export default Components_Bars_Next_Button