import React from 'react'

const Components_Bars_List = ({ data, selected, selectBar }) => {
    if(data.length > 0) {
        return (
            <div className="list-group">
                {data.map((bar, i) => (
                    <button
                        key={i}
                        type="button"
                        onClick={() => selectBar(bar.id)}
                        className={`
                            list-group-item
                            list-group-item-action
                            ${(selected === bar.id ? 'active' : '')}
                        `}
                    >{bar.name}</button>
                ))}
            </div>
        )
    } else {
        return <div>No bars found</div>
    }
}

export default Components_Bars_List