import React from 'react'

const Components_Bars_Search = ({ filterBars }) => (
    <input
        type="text"
        placeholder="Find bars"
        className="form-control bars__filter"
        onChange={filterBars}
    />
)
    

export default Components_Bars_Search