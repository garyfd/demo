import React from 'react'
import { Redirect } from 'react-router-dom'

const Components_Welcome = ({ order, createOrder }) => {
    if(order.id) return <Redirect to={`/bars/${order.id}`} />

    return (
        <div>
            <h1>Welcome</h1>
            <button className="btn btn-primary" onClick={createOrder}>Create new order</button>
        </div>
    )
}

export default Components_Welcome