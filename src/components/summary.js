import React from 'react'
import Templates_Loading from '../templates/loading'

const Components_Summary = ({ order, loadOrder }) => {

    if(!order.loaded) {
        loadOrder(order.id)
        return <Templates_Loading />
    }

    return (
        <div>
            <h1>Order Summary</h1>

            <p>Total: £{order.total.toFixed(2)}</p>

            <ul className="list-group">
                {order.drinks.map((drink, i) => (
                    <li key={i} className="list-group-item">
                        £{drink.current_price} - {drink.name}
                    </li>
                ))}
            </ul>
            
            <a className="btn btn-primary" href='/'>Start again</a>
        </div>
    )
}

export default Components_Summary