import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom"

import Containers_Welcome from './containers/welcome'
import Containers_Summary from './containers/summary'
import Containers_Bars from './containers/bars'
import Containers_Drinks from './containers/drinks'

const Routes = () => (
    <Router>
        <Switch>
            <Route
                exact
                path='/'
                component={Containers_Welcome}
            />

            <Route
                exact
                path='/bars/:orderId'
                component={Containers_Bars}
            />

            <Route
                exact
                path='/drinks/:orderId'
                component={Containers_Drinks}
            />

            <Route
                exact
                path='/summary/:orderId'
                component={Containers_Summary}
            />
        </Switch>
    </Router>
)

export default Routes