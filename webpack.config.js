var webpack = require('webpack');

module.exports = {
    entry: {
        app: "./src/app.js",
    },
    output: {
        path: __dirname+"/public",
        filename: "[name].js"
    },
    module: {
        loaders: [
            {
                test: /\.js(x?)$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            },
        ]
    }
};