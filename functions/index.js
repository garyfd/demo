const functions = require('firebase-functions')
const admin = require('firebase-admin')
const cors = require('cors')({ origin: true })

admin.initializeApp(functions.config().firebase)

exports.bars = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let bars = []
        admin.database().ref('bars').once('value')
            .then(snap => {
                let result = snap.val()
                result.map(b => bars.push(b))
                res.send({bars})
            })
    })
})

exports.drinks = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let drinks = []
        admin.database().ref(`drinks/${req.query.bar}`).once('value')
            .then(snap => {
                let result = snap.val()
                if(result) {
                    result.map(d => drinks.push(d))
                    return res.send({drinks})
                }
                return res.status(404).send({ error: 'no drinks found', drinks })
            })
    })
})

exports.load = functions.https.onRequest((req, res) => {
    cors(req, res, () => {

        if(!req.query.id) return res.status(400).send({ error: 'ID is required' })

        admin.database().ref(`orders/${req.query.id}`).once('value')
            .then(snap => {
                let order = snap.val()
                if(order) return res.send({ order: snap.val() })
                return res.status(404).send({ error: 'order not found', order })
            })
            .catch(err => {
                return res.status(500).send(err)
            })
    })
})

exports.create = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        let orderKey = admin.database().ref(`orders`).push()
        let order = { id: orderKey.key, total: 0 }
        orderKey.set(order)
        return res.send(order)
    })
})

exports.save = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        if(!req.query.id) return res.status(400).send({ error: 'ID is required' })
        admin.database().ref(`orders/${req.query.id}`).set(req.body)
        return res.send({
            status: 'ok',
            order: req.body
        })
    })
})