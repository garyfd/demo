# Technology
- JavaScript
- React.js
- Redux
- Bootstrap
- Webpack
- Firebase (Demo API)

# Demo
https://fddemo-5c0cd.firebaseapp.com

# API Requirements

## GET /create
Creates a new order and returns the ID
### Example Request
```json
{}
```

### Example Response
```json
{
    "id": "-L6DXiKuJ_QUQw2k2mv5"
}
```

## GET /bars
Returns a list of bars within X miles of the users location
### Example Request (query string)
```json
{
    "lat": 123.332131,
    "lon": 4.23221
}
```

### Example Response
```json
{
    "bars": [
        {
            "id": 1,
            "name": "Corstorphine Inn",
            "image_url": "https://......",
            "lat": 123,
            "lon": 123
        },
        {
            "id": 2,
            "name": "The White Lady",
            "image_url": "https://......",
            "lat": 456,
            "lon": 123
        }
    ]
}
```

## GET /drinks (query string)
Returns the available drinks for a bar
### Example Request
```json
{
   "bar": "1" 
}
```

### Example Response
```json
{
    "drinks": [
        {
            "id": 1,
            "name": "Tennents Lager",
            "current_price": 2.99,
            "image_url": "https://...."
        },
        {
            "id": 2,
            "current_price": 4.49,
            "name": "Brooklyn Lager",
            "image_url": "https://...."
        },
    ]
}
```

## GET /load
Loads the data for a specific order
### Example Request
```json
{
   "id": "-L6DWAXURULXzLcclLXe" 
}
```

### Example Response
```json
{
    "order": {
        "bar": 1,
        "drinks": [
            {
                "current_price": 2.99,
                "id": 1519589088274,
                "image_url": "abc",
                "name":"Tennents Lager"
            },
            {
                "current_price": 4.49,
                "id": 1519589089482,
                "image_url": "abc",
                "name": "Brooklyn Lager"
            }
        ],
        "id": "-L6DWAXURULXzLcclLXe",
        "total": 7.48
    }
}
```

## POST /save
Saves the data for a specific order
### Example Request
```json
{
    "id":"-L6DWAXURULXzLcclLXe",
    "bar":1,
    "total":10.47,
    "drinks":[
        {
            "current_price":2.99,
            "id":1519589088274,
            "image_url":"abc",
            "name":"Tennents Lager"
        },
        {
            "current_price":4.49,
            "id":1519589089482,
            "image_url":"abc",
            "name":"Brooklyn Lager"
        }
    ]
}
```

### Example Response
```json
{
    "status": "ok"
}
```